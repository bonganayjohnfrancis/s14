function addNum(x,y,z){
		return x + y + z;
	};
let addResult = addNum(8, 3, 4);
	console.log("The sum of x, y and z is " + addResult);

function subNum(x,y,z){
		return x - y - z;
	};
let subResult = subNum(8, 3, 4);
	console.log("The difference of x, y and z is " + subResult);

function mulNum(x,y,z){
		return x * y * z;
	};
let mulResult = mulNum(8, 3, 4);
	console.log("The product of x, y and z is " + mulResult);

let product = ["The product cost " + mulResult +" pesos"];
console.log(product);